# \<sangha-contributors\>

Show name and image of contributors.

```html
<sangha-contributors
  contributors='[{"avatar":"avatar_url", "name":"the_name"}]'
  max-contributors="10 (optional)"
  small="true (optional to show smaller images)"
  large="true (optional to show larger images)"
  overlap="true (optional to overlap the images)"
  img-size="100 (optional)"
></sangha-contributors>
```

## Development

```bash
# Get dependencies
$ npm install

# Demo site
$ npm start

# Run tests
$ npm test
```
